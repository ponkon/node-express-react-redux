# Node Express React Redux Skeleton

Baseline Node Express React Redux appliation.

## Development using docker

### Prerequisites

- A linux host environment (Ubuntu Trusty 64 LTS)
- docker
- docker-compose
- node
- gulp

#### Install docker

https://docs.docker.com/engine/installation/ubuntulinux/

#### Install docker-compose

https://docs.docker.com/compose/install/

#### Install node

https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions

#### Install gulp 

`$ sudo npm install -g gulp`

### Project setup

`$ docker-compose build`

Run the docker container

`$ docker-compose up -d`

The app should now be running. Visit http://localhost/ to verify that it's running.

### Copy files across from local system (Run on local machine)
```
cd ~/node-express-react-redux/client
gulp
cd ~/node-express-react-redux
node publish.js <IP ADDRESS>
```
