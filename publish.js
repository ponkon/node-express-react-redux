//make sure the bundle.js has been properly built first.
//use supervisor to start index.js - that way it'll watch for changes.
//Currently supervisor checks for .node and .js files - can change that if needed though.
  //supervisor index.js

var scpClient = require('scp2');

scpClient.scp('./server/', {
  host: process.argv[2] || '192.168.0.49',
  username: process.argv[3],
  password: process.argv[4],
  path: process.argv[5]
}, function(err) {});

