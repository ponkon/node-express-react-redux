export const styleConstants = {
  color : {
    primaryOrange: '#fc5014',
    secondaryOrange: '#fdca3b',
    tertiaryOrange: '#e7c47f',
    primaryBlack: '#171717',
    primaryBlue: '#0e82ff'    
  }
}