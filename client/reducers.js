import { combineReducers } from 'redux'
import _ from 'lodash'
import { 
  ADD_ITEM
} from './actions.js'



function item (state, action) {
  let result = state;
	if (action.type === ADD_ITEM) {
    result = {
      id: action.id,
      text: action.text
    }
  }

  return result
}

function items(state = [], action) {
  let result = state 

  if (action.type === ADD_ITEM) {
    result = [
      ...state,
      item(undefined, action)
    ];
  }

  return result;
}

const nodeExpressReactReduxApp = combineReducers({
  items
})

export default nodeExpressReactReduxApp