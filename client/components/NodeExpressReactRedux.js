
'use strict';

import React, { Component, PropTypes } from 'react'
import _ from 'lodash'

import { styleConstants } from 'styleConsts'

export class NodeExpressReactRedux extends Component {

  render() {
    const { 
      items,
      handleAddItem
    } = this.props;

    let styles = this.constructor.styles.all;

    return (
      <div style={styles.body}>
        <h1 style={styles.header}>Welcome to the Node Express React Redux Skelton App</h1>
        <AddItem handleAddItem={handleAddItem} />
        <ItemList items={items} />
      </div>
    );
  }
}

NodeExpressReactRedux.propTypes = {
  items: PropTypes.array.isRequired
}

NodeExpressReactRedux.styles = {
  all: {
    body: {
      backgroundColor: styleConstants.color.primaryBlack,
      fontFamily: '\'Share Tech Mono\', Courier, monospace'
    },
    header: {
      fontSize: '42px',
      color: styleConstants.color.primaryOrange,
      textAlign: 'center'
    },
    list: {
      listStyle: 'none'
    },
  }
}


export class AddItem extends Component {
  handleClick(e) {
    const { handleAddItem } = this.props;

    const node = this.refs.input;
    const text = node.value.trim();
    handleAddItem(text);
    node.value = '';
  }

  render() {
    const {
      handleAddItem,
    } = this.props;

    const styles = _.defaultsDeep({}, this.constructor.styles.all);

    return (
      <div>
        <input type="text" ref="input" />
        <button onClick={e => this.handleClick(e)}>Add</button>
      </div>
    )
  }
}

AddItem.propTypes = {
  handleAddItem: PropTypes.func.isRequired
}

AddItem.styles = {
  all: {
    input: {

    },
    button: {

    }
  }
}

export class Item extends Component {
  render() {
    const {
      item,
    } = this.props;

    const styles = _.defaultsDeep({}, this.constructor.styles.all);

    return (
      <li style={styles}>{item.text}</li>
    )
  }
}

Item.propTypes = {
  item: PropTypes.object.isRequired
}

Item.styles = {
  all: {
  }
}

export class ItemList extends Component {
  render() {
    const {
      items,
    } = this.props;

    const styles = _.defaultsDeep({}, this.constructor.styles.all);

    return (
      <ul style={styles}>
        {_.map(items,
          (item, index) => 
            <Item 
              item={item}
              key={item.id} />
          )
        }
      </ul>
    )
  }
}

ItemList.propTypes = {
  items: PropTypes.array.isRequired
}

ItemList.styles = {
  all: {
    listStyleType: 'none',
    fontSize: '32px',
    color: styleConstants.color.primaryBlue,
  }
}