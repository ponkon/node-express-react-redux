'use strict';

import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'

import { NodeExpressReactRedux } from 'components/NodeExpressReactRedux'

import { 
  addItem
} from '../actions'

class NodeExpressReactReduxContainer extends Component {

  handleAddItem(text) {
    const { dispatch } = this.props;

    dispatch(addItem(text));
  }

  render() {
    const { 
      items
    } = this.props;

    return (
      <NodeExpressReactRedux 
        items={items}
        handleAddItem={this.handleAddItem.bind(this)} />
    )
  }
}

export default connect((state) => state)(NodeExpressReactReduxContainer)