'use strict';

import React from 'react';
import _ from 'lodash';
import { render } from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
import createLogger from 'redux-logger';
import * as Actions from './actions';
import nodeExpressReactReduxApp from './reducers';

import NodeExpressReactReduxContainer from './components/NodeExpressReactReduxContainer';

const logger = createLogger();
const createStoreWithMiddleware = applyMiddleware(thunk, logger, promise)(createStore);
const store = createStoreWithMiddleware(nodeExpressReactReduxApp);

let rootEl = document.getElementsByClassName('node-express-react-redux-app')[0];

render(
	<Provider store={store}>
	  <NodeExpressReactReduxContainer />
	</Provider>,
	rootEl
);