'use strict';

var gulp = require('gulp'),
    browserify = require('browserify'),
    gutil = require('gulp-util'),
    watchify = require('watchify'),
    transform = require('vinyl-transform'),
    _ = require('lodash'),
    babelify = require('babelify'),
    bust = require('gulp-buster'),
    rename = require('gulp-rename'),
    args = require('yargs').argv,
    doWatch = args.watch,
    typeArg = gutil.env.type || 'debug',
    buildConfig = typeArg.toLowerCase(),
    doDebug = buildConfig == 'debug';

// create javascript bundle
var bundle = function () {
  var browserified = transform(function (filename) {
    return bundler.bundle();
  });

  return gulp.src(bundleEntryPoint)
    .pipe(browserified)
    .pipe(rename('bundle.js'))
    .pipe(doDebug ? gutil.noop() : uglify())
    .pipe(gulp.dest('../server/dist/'))
    .pipe(bust())
    .pipe(gulp.dest('../server/dist/'));
};

var bundleEntryPoint = './index.js';

var browserifyArgs = {
  debug: doDebug,
  paths: ['./']
};

// use the watchify bundler to recreate our bundle when js files change
var bundler = browserify(bundleEntryPoint,
                         _.extend(watchify.args, browserifyArgs))
  .transform(babelify.configure({
    // don't babelify our vendor scripts
    ignore: ['./node_modules', './vendor'],
    presets: ['es2015'],
    plugins: ['transform-object-assign', 'transform-react-jsx']
  }));

if (doWatch) {
  bundler = watchify(bundler);
  bundler.on('update', bundle);
}

gulp.task('browserify', bundle);

//Should find a better way to watch all files except node_modules
gulp.task('watch', function() {
  gulp.watch(['./index.js', './reducers.js', './actions.js', './components/*.js']);
});

var defaultTasks = ['browserify'];
if (doWatch) defaultTasks.push('watch');

gulp.task('default', defaultTasks);